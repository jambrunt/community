# Content Moderation

Our wish is that we can be a low moderation instance, however as stated in our rules & detailed within these documents - this is not a “free for all”.

While freedom of expression is indeed a foundational right and as such it is essential for the enjoyment and protection of all human rights, it is not absolute. 
When posts contain material that could be demeaning, disrespectful and/or downright offensive towards individuals or a specific group of, then this right is no longer valid and will be subject to moderation & consequences.

We want the instance to be used by anyone and that they feel safe that they will be able to shield themselves from any sort of content they wish not to see.

## Difference Of Opinions

Individuals aren’t always going to see eye to eye and have differing opinions on a number of topics.
This is totally fine, and somewhat encouraged to foster some healthy debate and conversations.
Users have the control of when to ignore/mute/block other users when they feel that those conversations and those debates become too heated. 
All other parties involved must respect that at all times.

## NSFW/Adult (18+)/Sensitive Material

Any posts with content that is classed as inappropriate for reading/viewing in a professional setting (NSFW), contain topics/images of an adult nature or that may invoke polarised opinions, trauma, phobias, etc must be marked as sensitive with text and/or any appropriate hashtags within the post & its content warning clearly defining what it is.
Doing so would allow individuals to place filters in order to minimise their exposure to unwanted content and have a better experience. 

## Hateful/Derogatory/Disrespectful

Any posts or user accounts brought to our attention that contains directly or indirectly (linked) material that encourages or advocates violence or any discrimination against people (individual or as a group) will not be tolerated.

