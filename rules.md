---
title: Our Rules
---

# Rules

These are the rules that are currently in operation on our instance.
We regularly review them and any changes will be documented here as well as announced on the instance.

* Be Kind and Respectful. Don't be racist, sexist, homophobic, transphobic, ableist, fatphobic, etc.

* Any discriminatory, threatening or harassment behaviour or content promoting or advocating said behaviours will not be tolerated.

* Keep Your Content LEGAL! Don't do/discuss doing/link to anything illegal.

* Must consider others at all times. Include relevant #HashTags to aid discoverability & filtering potenital and add image descriptions (Alt-TXT).

* Use of a Content Wrapper/Warning (CW) must be utilised for topics and/or images that may invoke a strong difference of opinion or could be distressing to some (for example: phobias), as well as lewd, suggestive or adult themed imagery or text.

* Credit other artists/photographers/creators appropriately when posting their work whenever possible.

* No unsolicited / spam posts permitted. This includes commercial advertising, promotional campaigns, etc.

* No impersonation of an entity or another individual. Your identity must be your own, whether this is yourself or your online persona.

---

Wish to seek clarification, suggest new or amend existing rules or just want to open up discussions about them?

Open up an issue on our GitLab repository: [cr8r/community](https://gitlab.com/cr8r/community/-/issues/new)