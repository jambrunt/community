---
title: Goverance
---

# Governance

While the day to day operations are the responsibility of the Moderation Team, the instance is governed by those who use and support it.

As the instance grows, requests/suggestions etc can be submitted to help shape our future within our own little community aswell as the wider fediverse.

You can DM [@CR8R](https://cr8r.gg/@CR8R), email [support@cr8r.gg](mailto:support@cr8r.gg) or use our [GitLab Issue Tracker](https://gitlab.com/cr8r/community/-/issues) 

## Moderation Team

### Admins

> [@GunFheum](https://cr8r.gg/@GunFheum)

> [@SmuggieStreams](https://cr8r.gg/@SmuggieStreams)

### Moderators

> _While not actively looking to add Moderators at this time, as our numbers grow we will be looking for assistance._

